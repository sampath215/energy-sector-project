# Energy sector project

A Transportation Distribution Charge is a charge levied by the Distribution companies for the use of their lower pressure pipelines. The charge covers the cost of physically transporting the gas through the pipeline. The cost is variable in time and is determined by the Exit Zone (a regional code) and the (estimated) rolling consumption quantity of the meter.

Here we make a Python program to calculate it and use Machine Learning to make mock forecasts

